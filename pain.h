#pragma once

#include <cnum.h>

long double ldUniform();

Complex MarsagliaGaussian(Complex mean, long double stddev);
