#include <cli.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static const char* help_page =
"		Buddhabrotter, the Bhuddabrot Plotter"			"\n"
"Usage:	%s [OPTIONS] output-file-path"					"\n"
									"\n"
"Creates a PGM image and periodically updates it as more points are"	"\n"
"processed. And from there you may pause the processing by raising"	"\n"
"SIGINT, eg. pressing CTRL-C, from there you can abort or continue the"	"\n"
"render."								"\n"
									"\n"
"Here “abort” is a bit misleading, it does still save all progress if"	"\n"
"you actually select the abort option."					"\n"
									"\n"
"  -h, --help			display the help page, aka this!"	"\n"
									"\n"
"  -o, --origin=%%Lf+i%%Lf\t	complex number on the image's center"	"\n"
"  -t, --nthreads=%%ld		the number of _worker_ threads"		"\n"
"  -i, --max-iter=%%ld		max iterations for the fractal"		"\n"
"  -d, --img-size=%%ldx%%ld	width and height of the output image"	"\n"
"  -s, --scale=%%Lf		image scale, small → zoom"		"\n"
"  -p, --save-period=%%ld	seconds between save attempts"		"\n"
									"\n"
"Be advised, since I'm supremely lazy and parsed `argv` with sscanf,"	"\n"
"the parser gets really sad if put spaces between the arguments etc."	"\n";

Settings read_args(
	int argc,
	char const* const * argv,
	size_t *num_threads,
	char const* * name
) {
	Settings settings;
	settings.max_iter = 1024;
	settings.width    = 1024;
	settings.height   = 1024;
	settings.savep    = 60;
	settings.origin   = Complex_zero();
	settings.scale    = 3.5;
	
	*num_threads = 1;

	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--help") * strcmp(argv[i], "-h") == 0) {
			printf(help_page, argv[0]);
			exit(0);
		}
	}

	for (int i = 1; i < argc - 1; i++) {
		char const * arg = argv[i];

		size_t matches = 0;

		matches += sscanf (arg, "--threads    =%ld", num_threads);
		matches += sscanf (arg,        "-t    =%ld", num_threads);

		matches += sscanf (arg, "--max-iter   =%ld", &settings.max_iter);
		matches += sscanf (arg, "-i           =%ld", &settings.max_iter);

		matches += sscanf (arg, "--img-size   =%ldx%ld",
		 	&settings.width, &settings.height);
		matches += sscanf (arg, "-d           =%ldx%ld",
		 	&settings.width, &settings.height);

		matches += sscanf (arg, "--origin     =%Lf+i%Lf",
	 		&settings.origin.a, &settings.origin.b);
		matches += sscanf (arg, "-o           =%Lf+i%Lf",
			&settings.origin.a, &settings.origin.b);

		matches += sscanf (arg, "--scale      =%Lf", &settings.scale);
		matches += sscanf (arg, "-s           =%Lf", &settings.scale);

		matches += sscanf (arg, "--save-period=%ld", &settings.savep);
		matches += sscanf (arg, "-p           =%ld", &settings.savep);

		if (matches == 0) {
			printf ("Error: unrecognized argument \n"
				"“%s”\n", arg);
			printf(help_page, argv[0]);
			exit(-i);
		}
	}

	*name = argv[argc-1];
	
	if (argc < 2) {
		printf("Error: expected at least one argument\n");
		printf(help_page, argv[0]);
		exit(2 + argc);
	}
	
	return settings;
}
