#include <worker.h>
#include <pain.h>
#include <cnum.h>

#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

static Complex next_step(Complex z, Complex start) {
	Complex s;

	s.a = z.a * z.a - z.b * z.b + start.a;
	s.b = 2   * z.a * z.b       + start.b;

	return s;
}

Input mk_Input(Settings const* settings, size_t *hits) {
	Input i;
	i.dying    = 0;
	i.stopped  = 0;
	i.points   = 0;
	i.hits     = hits;
	i.settings = settings;

	return i;
}

long double transition_prob (size_t old_len, size_t new_len, size_t max) {
	return
		(1-(max - old_len) / (long double) max )/
		(1-(max - new_len) / (long double) max ); 
}

void* worker(void* _ptr) {
	Input* ptr = (Input*) _ptr;
	Settings const* settings = ptr->settings;

	Complex *orbit = calloc(settings->max_iter, sizeof(Complex));

	Complex seed;
	size_t  seed_len = 0;
	int   first_time = 1;
	do {
		while (ptr->stopped)
			usleep(50000);

		Complex start, current;

		if (ldUniform() > 0.99) {
			first_time = 1;
		}

		if (first_time) {
			seed_len   = 0;
			start = randz();
		} else {
			start = MarsagliaGaussian(seed, 0.01);
		}

		current.a = //...
		current.b = 0;
		
		size_t iter;
		for (iter = 0; iter < settings->max_iter; iter++) {
			current     = next_step(current, start);
			orbit[iter] = current;

			if (current.a*current.a + current.b*current.b >= 4)
				break;
		} 
		
		if (iter == settings->max_iter)
			continue;

		if (first_time) {
			seed       = start;
			seed_len   = iter;
			first_time = 0;
		} 
		
		for (size_t j = 1; j <= iter; j++) {
			size_t *hit = find_hit(
				orbit[j],
				settings->origin,
				settings->scale,
				settings->width,
				settings->height,
				ptr->hits
			);

			if (hit != NULL) {
				*hit += 1;
			}
			
			orbit[j].b *= -1;
			hit = find_hit(
				orbit[j],
				settings->origin,
				settings->scale,
				settings->width,
				settings->height,
				ptr->hits
			);
		
			if (hit != NULL) {
				*hit += 1;
			}
		}
		
		long double ratio = transition_prob(
			seed_len, iter, settings->max_iter);

		if (ldUniform() > ratio) {
			seed     = start;
			seed_len = iter;
		}

		ptr->points++;
	} while (!ptr->dying);

	free(orbit);
	return NULL;	
}
