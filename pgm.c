#define PGM_MAX 65535

#include <pgm.h>

#include <stdio.h>
#include <math.h>

void print_to_file (
	char   const*		name, 
	unsigned short const*	vals, 
	size_t w, size_t h
) {
	FILE* image = fopen(name, "w");

	fprintf(image, "P5 %d %d %d\n", (int)w, (int)h, PGM_MAX);
	fwrite (vals, sizeof(unsigned short), w * h, image);

	fclose(image);
}


long double bias(long double x, long double val) {
	return (val > 0) ? powl(x, logl(val) / logl(0.5)) : 0;
}

long double gain(long double x, long double val ) {
	return 0.5 * ((x < 0.5) ? bias (2*x, 1-val): (2 - bias(2-2*x,1-val)));
}

void normalize_hits(
	size_t const   *hits, 
	unsigned short *vals, 
	size_t w, size_t h
) {
	size_t max = 0;
	for (size_t t = 0; t < w*h; t++) {
		if (hits[t]>max)
			max = hits[t];
	}

	long double factor = (long double) PGM_MAX / (max+1);
	
	for (size_t t = 0; t < w*h; t++) {
		if (hits[t] * factor > PGM_MAX)
			vals[t] = PGM_MAX;
		else 
			vals[t] = (unsigned) (hits[t] * factor);
	}
}
