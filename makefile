.PHONY: all clean

SOURCES = $(shell find -name "*.c")
HEADERS = $(shell find -name "*.h")
OBJECTS = $(subst %.c, %.o, $(SOURCES))

CC = gcc -I. -Ofast -Wall -Wextra

all: buddha.out
	@echo done

clean: 
	find -name "*.o"   -exec rm {} \;
	find -name "*.out" -exec rm {} \;

%.out:   $(OBJECTS)
	$(CC) -o    $@ $^ -lpthread -lm

%.o: %.c $(HEADERS)
	$(CC) -c -o $@ $<
