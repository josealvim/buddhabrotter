#include <worker.h>
#include <cnum.h>
#include <cli.h>
#include <pgm.h>

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <signal.h>
#include <pthread.h>

static Input*       input = NULL;
static pthread_t* threads = NULL;
static size_t num_threads = 0;
static int         dying  = 0;

void  spawn_threads();
void resume_threads();
void  pause_threads();
void   stop_threads();

void term_handler(int dummy);
void erase_line();

size_t count_points();

static unsigned short  *values;
static size_t          *hits;
static Settings         settings;
static char const*      name;

int main (int argc, char** argv) {
	srand(time(0));
	
	settings = read_args(
		argc, (char const* const*) argv, &num_threads, &name);

	values = calloc(
		settings.width*settings.height, sizeof(unsigned short));
	hits = calloc(
		settings.width*settings.height, sizeof(__int128 unsigned));
	input = calloc(
		num_threads, sizeof(Input));
	threads = calloc(
		num_threads, sizeof(pthread_t));

	for (size_t t = 0; t < num_threads; t++)
		input[t] = mk_Input(&settings, hits);

	spawn_threads();
	
	signal(SIGINT, term_handler);

	time_t prev  = time(0);
	time_t now   = time(0);
	time_t start = now;

	size_t points_recent[10] = {};

	while (!dying) {
		now = time(0);

		size_t index = (now - start) % 10;
		size_t points_now = count_points();
		points_recent [index] = points_now;
		
		double pps = 0;
		for (int i = 0; i < 10; i++) {
			long long diff = 
				points_recent[(index+i)%10] -
				points_recent[(index+9+i)%10];
			
			if (diff < 0)
				continue;

			pps += diff;
		}
		pps /= 10;

		erase_line();
		printf ("%ld points processed (%.2f p/s)", 
			points_now, pps);

		if ((size_t)(now - prev) > settings.savep) {
			normalize_hits(
				hits, values, 
				settings.width, settings.height);
			
			print_to_file (
				name, values, 
				settings.width,	settings.height);

			prev = time(0);
		}

		usleep(100000);
	}
	
	stop_threads();
	
	normalize_hits(
		hits, values, 
		settings.width, settings.height);
			
	print_to_file (
		name, values, 
		settings.width,	settings.height);

	free (threads);
	free (values);
	free (input);
	free (hits);

	printf("\n");

	return 0;
}

void spawn_threads() {
	sigset_t s;
	sigemptyset(&s);
	sigaddset(&s, SIGINT);
	sigprocmask(SIG_BLOCK, &s, NULL);

	for (size_t t = 0; t < num_threads; t++){
		pthread_create (
			&threads[t],
			NULL, 
			worker, 
			(void*) &input[t]
		);
	}
	
	sigprocmask(SIG_UNBLOCK, &s, NULL);
}

void resume_threads() {
	for (size_t t = 0; t < num_threads; t++) 
		input[t].stopped = 0;
}

void pause_threads() {
	for (size_t t = 0; t < num_threads; t++) 
		input[t].stopped = 1;
}

void stop_threads() {
	for (size_t t = 0; t < num_threads; t++) 
		input[t].dying = 1;
}

void term_handler(int dummy) {
	(void) dummy;
	
	pause_threads();

	printf(	"\nReceived SIGINT: \n" 
		"- ‘c’ to [c]ontinue\n"
		"- ‘a’ to [a]bort   \n"
		"- ‘p’ to [p]rint   \n"
		"> "
	);

	while (1) {
		char c;
		scanf("%c", &c);
		switch(c) {
			case 'p':
				normalize_hits(
					hits, values, 
					settings.width, settings.height);
			
				print_to_file (
					name, values, 
					settings.width,	settings.height);

				printf("printed\n> ");
				continue;

			case 'c':
				resume_threads();
				return;

			case 'a':
				dying = 1;
				return;

			case '\n': 
				continue;

			default:
				printf("unrecognized option ‘%c’\n> ", c);
		}
	}
}

void erase_line() {
	printf("\r");
}

size_t count_points() {
	size_t total = 0;
	for (size_t t = 0; t < num_threads; t++)
		total += input[t].points;
	return total;
}


