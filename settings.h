#pragma once

#include <cnum.h>

#include <stddef.h>

typedef struct {
	size_t max_iter;
	size_t width, height;
	size_t savep;

	Complex origin;
	long double scale;
} Settings;
