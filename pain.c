#include <pain.h>

#include <stdlib.h>
#include <math.h>

long double ldUniform() {
	return (long double) random() / 0x7FFFFFFF;
}

Complex MarsagliaGaussian(Complex mean, long double stddev) {
	long double u, v, s;
	do {
		u = ldUniform() * 2 - 1;
		v = ldUniform() * 2 - 1;
		s = u * u + v * v;
	} while (s >= 1.00 || s == 0);
	
	s = sqrtl(-2 * logl(s) / s);

	Complex z;
	z.a = mean.a + u * stddev;
	z.b = mean.b + v * stddev;
	
	return z;
}
