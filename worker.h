#pragma once

#include <cnum.h>
#include <settings.h>

#include <stddef.h>

typedef struct {
	size_t  points;
	size_t *hits;

	Settings const* settings;	

	int dying;
	int stopped;
} Input;

Input mk_Input(Settings const*, size_t* hits);

void* worker(void*);
