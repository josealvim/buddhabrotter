#pragma once

#include <stddef.h>

void print_to_file (
	char   const*, unsigned short const*, size_t w, size_t h);

void normalize_hits(
	size_t const*, unsigned short*, size_t w, size_t h);
