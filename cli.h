#pragma once

#include <settings.h>

Settings read_args(
	int argc,
	char const* const *argv,
	size_t *num_threads,
	char const* * name
);
