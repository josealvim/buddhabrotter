#pragma once

#include <stddef.h>

typedef struct {
	long double a, b;
} Complex;

Complex randz();

Complex Complex_zero();

size_t * find_hit(
	Complex z, 
	Complex o, 
	long double scale,
	size_t w, size_t h,
	size_t *hits
);
