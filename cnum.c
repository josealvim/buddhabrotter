#include <cnum.h>
#include <pain.h>
#include <stdlib.h>

Complex randz() {
	Complex z;
	z.a = (ldUniform() * 2 - 1) * 4;
	z.b = (ldUniform() * 2 - 1) * 4;
	return z;
}

Complex Complex_zero() {
	Complex z;	
	z.a = z.b = 0;
	return z;
}

size_t* find_hit(
	Complex z, 
	Complex o, 
	long double scale,
	size_t w, size_t h,
	size_t * hits
) {
	long double a =
		((z.a - o.a) / (2 * scale) + 0.5) * w;

	long double b =
		((z.b - o.b) / (2 * scale) + 0.5) * h;

	if (b < 0 || a < 0 || a > w || b > h)
		return NULL;
	else
		return &hits[w*(size_t)b + (size_t)a];
}
